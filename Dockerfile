# image de python utilisée
FROM python:3.9-slim-bullseye as base

# var d'environement pour python
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/app

WORKDIR /app

# Installation des dépendances
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    python3-cffi python3-brotli libpango-1.0-0 libpangoft2-1.0-0 \
    && rm -rf /var/lib/apt/lists/*

# Copie les fichiers
COPY . .

# installation des dépendances
RUN pip install --upgrade pip \
    && pip install -r requirements.txt

# Lancement des migrations (BD) puis lancement du projet
CMD python manage.py migrate && python manage.py runserver 0.0.0.0:8000