from django.urls import re_path

from . import consumers
from .room_jeu.consumers import RoomJeuConsumer

websocket_urlpatterns = [
    re_path(
        r"ws/ping/(?P<room_name>\w+)/$", consumers.PingPongConsumer.as_asgi()
    ),  # pour tester
    re_path(
        r"ws/partie/(?P<partie_code>\w+)/(?P<token>[\w-]+\.[\w-]+\.[\w-]+)/$",
        RoomJeuConsumer.as_asgi(),
    ),
]
