# Generated by Django 4.2.7 on 2023-12-14 09:03

import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
        ("WS_JEU", "0002_partie_atout_actuel_partie_donneur_partie_enchere_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="Joueur",
            fields=[],
            options={
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("auth.user",),
            managers=[
                ("objects", django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterField(
            model_name="partie",
            name="donneur",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="WS_JEU.joueur",
            ),
        ),
        migrations.AlterField(
            model_name="partie",
            name="equipe_1",
            field=models.ManyToManyField(
                related_name="parties_equipe_1", to="WS_JEU.joueur"
            ),
        ),
        migrations.AlterField(
            model_name="partie",
            name="equipe_2",
            field=models.ManyToManyField(
                related_name="parties_equipe_2", to="WS_JEU.joueur"
            ),
        ),
        migrations.AlterField(
            model_name="partie",
            name="joueurs",
            field=models.ManyToManyField(related_name="parties", to="WS_JEU.joueur"),
        ),
    ]
