# Generated by Django 4.2.7 on 2023-12-15 08:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("WS_JEU", "0004_partie_deck_partie_dernier_pli_partie_pli_actuel"),
    ]

    operations = [
        migrations.AddField(
            model_name="partie",
            name="etat",
            field=models.CharField(
                choices=[
                    ("ATTENTE", "ATTENTE"),
                    ("ENCHERES", "ENCHERES"),
                    ("JEU", "JEU"),
                    ("TERMINEE", "TERMINEE"),
                ],
                default="ATTENTE",
                max_length=10,
            ),
        ),
    ]
