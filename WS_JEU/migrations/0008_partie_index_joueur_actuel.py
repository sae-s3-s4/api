# Generated by Django 4.2.7 on 2023-12-18 16:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("WS_JEU", "0007_partie_joueurs_cartes"),
    ]

    operations = [
        migrations.AddField(
            model_name="partie",
            name="index_joueur_actuel",
            field=models.IntegerField(default=0),
        ),
    ]
