from django.contrib import admin

from WS_JEU.partie.models import Partie
from WS_JEU.joueur.models import Joueur
from WS_JEU.historique_partie.models import HistoriquePartie

# Register your models here.

admin.site.register(Partie)
admin.site.register(Joueur)
admin.site.register(HistoriquePartie)
