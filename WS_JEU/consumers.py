import json

from channels.generic.websocket import WebsocketConsumer


class PingPongConsumer(WebsocketConsumer):
    """
    Consumer pour tester les websockets
    """

    def connect(self):
        # on accepte toutes les connexions (pas de vérification de token)
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]
        nbr = text_data_json["nbr"]
        multiplier = text_data_json["multiplier"]

        multiplication = nbr * multiplier

        self.send(
            text_data=json.dumps(
                {
                    "message": message,
                    "type": "pong",
                    "nbr": nbr,
                    "multiplier": multiplier,
                    "multiplication": multiplication,
                }
            )
        )
