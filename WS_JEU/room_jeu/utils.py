from rest_framework_simplejwt.tokens import AccessToken

from WS_JEU.joueur.models import Joueur


def joueur_depuis_token(token):
    """
    Renvoie l'utilisateur à partir du token
    """
    token = AccessToken(token)
    joueur_id = token["user_id"]
    return Joueur.objects.get(id=joueur_id)
