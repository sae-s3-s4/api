class CoupInterditException(Exception):
    """
    Exception levée lorsque le coup joué par un joueur est interdit
    """

    def __init__(self, message="Coup interdit"):
        self.message = message
        super().__init__(self.message)
