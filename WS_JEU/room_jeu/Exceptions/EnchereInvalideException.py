class EnchereInvalideException(Exception):
    """
    Exception levée lorsque l'enchère jouée par un joueur est invalide
    """

    def __init__(self, message="Enchère invalide"):
        self.message = message
        super().__init__(self.message)
