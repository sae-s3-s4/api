import json

from channels.generic.websocket import JsonWebsocketConsumer, WebsocketConsumer
from asgiref.sync import async_to_sync
from rest_framework_simplejwt.tokens import AuthUser

from WS_JEU.joueur.models import Joueur
from WS_JEU.partie.carte import Carte
from WS_JEU.partie.models import Partie
from WS_JEU.partie.views import PartieSerializer
from WS_JEU.room_jeu.utils import joueur_depuis_token


class RoomJeuConsumer(JsonWebsocketConsumer):
    """
    Consumer la gestion de la room de jeu
    """

    def __init__(self, *args, **kwargs):
        super().__init__(args, kwargs)
        self.partie_code = None
        self.nom_groupe_partie = None
        self.partie: Partie = None
        self.user: Joueur = None

    def connect(self):
        # on accepte toutes les connexions (pas de vérification de token) pour l'instant
        self.accept()

        # on récupère l'utilisateur à partir du kwargs token
        token = self.scope["url_route"]["kwargs"]["token"]

        # on récupère l'utilisateur à partir du token
        try:
            self.user = joueur_depuis_token(token)
        except Exception as e:
            self.send_json({"erreur": "Token invalide", "code": "401"})
            self.close()
            return

        # on vérifie que l'utilisateur est bien connecté
        if not self.user.is_authenticated:
            self.send_json({"erreur": "Vous devez être connecté", "code": "401"})
            self.close()
            return

        # récupération du code de la partie dans l'url
        self.partie_code = self.scope["url_route"]["kwargs"]["partie_code"]
        print(self.partie_code)
        # on vérifie que la partie existe, sinon on envoie un message d'erreur et on ferme la connexion
        if not Partie.partie_existe(self.partie_code):
            self.send_json(
                {"type": "erreur", "erreur": "La partie n'existe pas", "code": "404"}
            )
            self.close()
            return

        # on récupère la partie
        self.nom_groupe_partie = f"coinche_{self.partie_code}"
        self.partie = Partie.objects.get(code=self.partie_code)

        # On vérifie qu'il n'y a pas déjà 4 joueurs dans la partie
        if not self.partie.partie_pleine() or self.user in self.partie.joueurs.all():
            if not self.user in self.partie.joueurs.all():
                # Rejoindre le groupe de la partie
                async_to_sync(self.channel_layer.group_add)(
                    self.nom_groupe_partie, self.channel_name
                )

                # On ajoute le joueur à la partie
                self.partie.ajouter_joueur(self.user)

                # On envoie un message à tous les joueurs de la partie pour les informer de la connexion
                async_to_sync(self.channel_layer.group_send)(
                    self.nom_groupe_partie,
                    {"type": "info.connexion"},
                )

                # On envoie l'état de la partie à tous les joueurs de la partie
                async_to_sync(self.channel_layer.group_send)(
                    self.nom_groupe_partie,
                    {"type": "etat.partie"},
                )
            else:
                # on reconnecte l'utilisateur et on lui envoie l'état de la partie
                async_to_sync(self.channel_layer.group_add)(
                    self.nom_groupe_partie, self.channel_name
                )

                # On envoie l'état de la partie à tous les joueurs de la partie
                async_to_sync(self.channel_layer.group_send)(
                    self.nom_groupe_partie,
                    {"type": "etat.partie"},
                )

        else:
            # On refuse la connexion si la partie est pleine
            self.close()

    def disconnect(self, code):
        """
        Déconnexion du joueur et rien d'autre
        """

        return super().disconnect(code)

    def receive_json(self, content, **kwargs):
        """
        Réception des messages websocket
        """
        type = content.get("type", None)

        if type == "message":
            # On envoie le message à tous les joueurs de la partie
            async_to_sync(self.channel_layer.group_send)(
                self.nom_groupe_partie,
                content,
            )

        elif type == "action.encherir":
            # on récupère l'enchère (valeur et famille)
            valeur = content.get("valeur", None)
            famille = content.get("famille", None)
            self.partie.annoncer_enchere(self.user, valeur, famille)
            self.partie.save()

            # On envoie l'état de la partie à tous les joueurs de la partie
            async_to_sync(self.channel_layer.group_send)(
                self.nom_groupe_partie,
                {"type": "etat.partie"},
            )

        elif type == "action.jouer":
            valeur = content.get("valeur", None)
            famille = content.get("famille", None)
            try:
                self.partie.jouer_carte(self.user, famille, valeur)
                self.partie.save()
                async_to_sync(self.channel_layer.group_send)(
                    self.nom_groupe_partie,
                    {"type": "etat.partie"},
                )
            except Exception as e:
                print(e)

        else:
            # on envoie un message d'erreur
            self.send_json({"error": "Action inconnue"})

    def info_connexion(self, event):
        """
        Envoie un message à tous les joueurs de la partie pour les informer de la connexion d'un joueur
        :return:
        """
        message = {
            "type": "info.connexion",
            "message": f"{self.user.username} a rejoint la partie",
            "nombre_joueurs": self.partie.nombre_joueurs(),
            "joueurs": [joueur.username for joueur in self.partie.joueurs.all()],
        }

        self.send_json(message)

    def info_deconnexion(self, event):
        """
        Envoie un message à tous les joueurs de la partie pour les informer de la déconnexion d'un joueur
        :param event:
        :return:
        """
        message = {
            "type": "info.deconnexion",
            "message": f"{self.user.username} a quitté la partie",
        }
        self.send_json(message)

    def etat_partie(self, event):
        """
        Envoie l'état de la partie à tous les joueurs de la partie
        :param event:
        :return:
        """
        self.partie.refresh_from_db()
        partie = PartieSerializer(self.partie).data
        if self.partie.etat != "ATTENTE":
            cartes = self.partie.joueurs_cartes[self.user.username]
            print(cartes)
            cartes_authorisees = [
                self.user.peut_jouer(
                    cartes,
                    self.partie.pli_actuel,
                    self.partie.atout_actuel,
                    Carte.from_json(carte),
                    False,
                )
                for carte in cartes
            ]

            partie["cartes_ok"] = cartes_authorisees
        doit_jouer = self.partie.a_son_tour(self.user)
        partie["doit_jouer"] = doit_jouer

        message = {
            "type": "etat.partie",
            "partie": partie,
            "username": self.user.username,
        }
        self.send_json(message)

    def message(self, event):
        """
        Echange de messages entre les joueurs
        :param event:
        :return:
        """
        self.send_json(event)
