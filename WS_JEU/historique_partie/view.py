from django.db import models
from django.db.models import Count, Q, OuterRef
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from WS_JEU.historique_partie.models import HistoriquePartie
from WS_JEU.joueur.models import Joueur
from WS_JEU.partie.models import Partie


class HistoriquePartieSerializer(serializers.ModelSerializer):
    partie = serializers.SlugRelatedField(slug_field="code", read_only=True)
    gagnants = serializers.SlugRelatedField(
        slug_field="username", many=True, read_only=True
    )
    perdants = serializers.SlugRelatedField(
        slug_field="username", many=True, read_only=True
    )

    class Meta:
        model = HistoriquePartie
        fields = [
            "date",
            "partie",
            "gagnants",
            "perdants",
            "points_gagnants",
            "points_perdants",
        ]


class HistoriqueUtilisateurView(APIView):
    def get(self, request, username):
        joueur = Joueur.objects.filter(username=username).first()
        if not joueur:
            return Response(
                {"message": "Joueur non trouvé"}, status=status.HTTP_404_NOT_FOUND
            )
        # requete pour recuperer les historiques de partie du joueur
        historiques = (
            HistoriquePartie.objects.filter(
                models.Q(gagnants=joueur) | models.Q(perdants=joueur)
            )
            .order_by("-date")
            .distinct()
        )
        serializer = HistoriquePartieSerializer(historiques, many=True)
        return Response(serializer.data)


class JoueurSerializer(serializers.Serializer):
    username = serializers.CharField()
    nombre_parties_gagnees = serializers.IntegerField()

class Classement(APIView):
    def get(self, request):
        #joueurc = Joueur.select
        #HistoriquePartie.objects.filter(gagnants__in=[joueurc]).delete()
        # Annoter chaque joueur avec le nombre de fois où il apparaît dans le champ gagnants
        parties_publiques = HistoriquePartie.objects.filter(partie__type='PUBLIQUE')

        # Requête pour récupérer les joueurs associés aux parties gagnées de type 'PUBLIQUE'
        subquery = parties_publiques.filter(gagnants=OuterRef('id'))

        # Requête principale pour annoter le nombre de parties gagnées de type 'PUBLIQUE' pour chaque joueur
        joueurs_annotated = Joueur.objects.annotate(
            nombre_parties_gagnees=Count('parties_gagnees', filter=Q(parties_gagnees__in=subquery))
        ).values('username', 'nombre_parties_gagnees').order_by('-nombre_parties_gagnees')
        # Serializer la liste annotée pour la réponse JSON
        serializer = JoueurSerializer(joueurs_annotated, many=True)
        return Response(serializer.data)

