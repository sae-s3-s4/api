from django.db import models
from django.contrib.auth.models import User
import random
import string

from WS_JEU.joueur.models import Joueur
from WS_JEU.partie.carte import Carte
from WS_JEU.partie.deck import Deck
import logging

logger = logging.getLogger(__name__)


class HistoriquePartie(models.Model):
    """Historique d'une partie"""

    date = models.DateTimeField(auto_now_add=True)
    partie = models.ForeignKey("Partie", on_delete=models.CASCADE)
    gagnants = models.ManyToManyField(
        Joueur, related_name="parties_gagnees", blank=True
    )
    perdants = models.ManyToManyField(
        Joueur, related_name="parties_perdues", blank=True
    )
    points_gagnants = models.IntegerField()
    points_perdants = models.IntegerField()
