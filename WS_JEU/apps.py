from django.apps import AppConfig


class WsJeuConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "WS_JEU"
