# django rest vue pour créer une partie
from rest_framework import generics, serializers, views, permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from WS_JEU.partie.models import Partie, generer_code_partie


# serializer partie
class PartieCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partie
        fields = ["code"]


class PartieSerializer(serializers.ModelSerializer):
    joueurs = serializers.SerializerMethodField()
    equipe_1 = serializers.SerializerMethodField()
    equipe_2 = serializers.SerializerMethodField()

    class Meta:
        model = Partie
        fields = [
            "etat",
            "code",
            "joueurs",
            "equipe_1",
            "equipe_2",
            "index_donneur",
            "pts_equipe_1",
            "pts_equipe_2",
            "terminee",
            "date_creation",
            "type",
            "enchere",
            "atout_actuel",
            "pli_actuel",
            "dernier_pli",
            "joueurs_cartes",
            "encheres_joueurs",
            "index_joueur_actuel",
        ]

    def get_joueurs(self, obj: Partie):
        return obj.ordre_joueurs()

    def get_equipe_1(self, obj: Partie):
        return [
            username
            for username in obj.equipe_1.all().values_list("username", flat=True)
        ]

    def get_equipe_2(self, obj: Partie):
        return [
            username
            for username in obj.equipe_2.all().values_list("username", flat=True)
        ]

class GetListeParties(views.APIView):
    serializer_class = PartieSerializer
    queryset = Partie.objects.all()

    def get(self, request, *args, **kwargs):
        parties_publiques = self.queryset.filter(type="PUBLIQUE")
        serialized_parties = self.serializer_class(parties_publiques, many=True).data
        return Response(serialized_parties, status=status.HTTP_200_OK)

class CreationPartiePrivee(views.APIView):
    serializer_class = PartieCreationSerializer
    queryset = Partie.objects.all()

    # permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        partie = Partie.creer_partie("PRIVEE")

        return Response(
            self.serializer_class(partie).data, status=status.HTTP_201_CREATED
        )


class CreationPartiePublique(views.APIView):
    serializer_class = PartieCreationSerializer
    queryset = Partie.objects.all()

    # permission_classes = [permissions.IsAuthenticated]

    def post(self, request, *args, **kwargs):
        partie = Partie.creer_partie()

        return Response(
            self.serializer_class(partie).data, status=status.HTTP_201_CREATED
        )
