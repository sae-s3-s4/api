class Carte:
    valeurs_non_atout = {
        1: 11,
        13: 4,
        12: 3,
        11: 2,
        10: 10,
        9: 0,
        8: 0,
        7: 0,
    }

    valeurs_atout = {
        1: 11,
        13: 4,
        12: 3,
        11: 20,
        10: 10,
        9: 14,
        8: 0,
        7: 0,
    }

    valeurs_tout_atout = {
        1: 7,
        13: 3,
        12: 2,
        11: 14,
        10: 5,
        9: 9,
        8: 0,
        7: 0,
    }

    valeurs_sans_atout = {
        1: 19,
        13: 4,
        12: 3,
        11: 2,
        10: 10,
        9: 0,
        8: 0,
        7: 0,
    }

    familles = ["coeur", "pique", "carreau", "trefle"]
    atouts = ["coeur", "pique", "carreau", "trefle", "tout_atout", "sans_atout"]

    def __init__(self, famille, valeur):
        self.famille = famille
        self._valeur = valeur

    @staticmethod
    def from_json(data):
        return Carte(data["famille"], data["valeur"])

    def __str__(self):
        return str(self._valeur) + " de " + str(self.famille)

    # Dans la classe Carte
    def __eq__(self, other):
        if isinstance(other, Carte):
            return self.famille == other.famille and self.valeur() == other.valeur()
        return False

    def get_famille(self):
        return str(self.famille)

    def valeur(self, atout=None):
        """
        Retourne la valeur de la carte en fonction de l'atout
        :param atout: atout actuel
        :return: valeur de la carte
        """

        if atout == "tout_atout":
            return self.valeurs_tout_atout[self._valeur]
        if atout == "sans_atout":
            return self.valeurs_sans_atout[self._valeur]
        if self.famille == atout:
            return self.valeurs_atout[self._valeur]
        else:
            return self.valeurs_non_atout[self._valeur]

    def superieur(self, other, atout, famille_premier=None):
        """
        Comparaison deux cartes avec self etant la carte la plus grande de la manche.
        Cette condition est nécessaire car durant une partie, on ne compare que la carte la plus grande de la manche avec la carte posée.
        :param self: la carte la plus grande de la manche
        :param other: l'autre carte
        :param atout: l'atout actuel
        :param premier: famille première carte posée
        :return: True si self est supérieur à other. False sinon
        """
        # si fait partie de la famille de la premiere carte posée et que l'autre ne fait pas partie de la famille de la premiere carte posée et n'est pas un atout
        if (
            self.famille == famille_premier
            and other.famille != famille_premier
            and other.famille != atout
        ):
            return True
        elif (
            self.famille != famille_premier
            and other.famille == famille_premier
            and self.famille != atout
        ):
            return False

        # si n'est pas un atout et que l'autre est un atout false
        if self.famille != atout and other.famille == atout:
            return False

        if self.famille == other.famille:
            if self.valeur(atout) == other.valeur(atout):
                return self._valeur > other._valeur
            else:
                return self.valeur(atout) > other.valeur(atout)
        else:
            if atout == "tout_atout" or atout == "sans_atout":
                return self.famille == famille_premier

            return self.famille == atout

    def json(self):
        return {"famille": self.famille, "valeur": self._valeur}
