from django.db import models
from django.contrib.auth.models import User
import random
import string

from WS_JEU.historique_partie.models import HistoriquePartie
from WS_JEU.joueur.models import Joueur
from WS_JEU.partie.carte import Carte
from WS_JEU.partie.deck import Deck
import logging

logger = logging.getLogger(__name__)


def pli_default():
    return {"joueurs": {}, "cartes": [], "couleur": None}


def generer_code_partie():
    """Génère un code de partie de 6 caractères aléatoires"""
    return "".join(random.choices(string.ascii_uppercase, k=6))


POINTS_MAX = 701


class Partie(models.Model):
    """Définition de la classe Partie"""

    TYPE_PARTIE = (
        ("PRIVEE", "PRIVEE"),
        ("PUBLIQUE", "PUBLIQUE"),
    )
    ATOUTS = (
        ("coeur", "coeur"),
        ("pique", "pique"),
        ("carreau", "carreau"),
        ("trefle", "trefle"),
        ("tout_atout", "tout_atout"),
        ("sans_atout", "sans_atout"),
    )

    ETAT_PARTIE = (
        ("ATTENTE", "ATTENTE"),
        ("ENCHERES", "ENCHERES"),
        ("JEU", "JEU"),
        ("TERMINEE", "TERMINEE"),
    )

    etat = models.CharField(
        max_length=10, choices=ETAT_PARTIE, default="ATTENTE", null=False, blank=False
    )
    code = models.CharField(max_length=6, unique=True)
    joueurs = models.ManyToManyField(Joueur, related_name="parties")
    deck = models.JSONField(default=list, null=True, blank=True)

    equipe_1 = models.ManyToManyField(Joueur, related_name="parties_equipe_1")
    pts_equipe_1 = models.IntegerField(default=0)

    equipe_2 = models.ManyToManyField(Joueur, related_name="parties_equipe_2")
    pts_equipe_2 = models.IntegerField(default=0)

    terminee = models.BooleanField(default=False)
    date_creation = models.DateTimeField(auto_now_add=True)

    # privée ou publique, les publiques seront listées dans la page d'accueil
    type = models.CharField(
        max_length=10, choices=TYPE_PARTIE, default="PUBLIQUE", null=False, blank=False
    )

    # deck = models.ManyToManyField("carte.Carte")
    # dernier_pli = models.ManyToManyField("carte.Carte")
    # carte_gagnante = models.ForeignKey("carte.Carte", null=True, blank=True)
    # index toujous entre 0 et 3
    index_donneur = models.IntegerField(default=0)
    index_joueur_actuel = models.IntegerField(default=0)

    enchere = models.IntegerField(default=0)
    atout_actuel = models.CharField(
        max_length=11, choices=ATOUTS, default=None, null=True, blank=True
    )
    encheres_joueurs = models.JSONField(default=dict, null=True, blank=True)

    # cartes sous format json
    pli_actuel = models.JSONField(default=pli_default, null=True, blank=True)
    dernier_pli = models.JSONField(default=list, null=True, blank=True)

    joueurs_cartes = models.JSONField(default=dict, null=True, blank=True)

    # code autogénéré pour rejoindre la partie

    def nombre_joueurs(self):
        """Renvoie le nombre de joueurs dans la partie"""
        return self.joueurs.count()

    def ajouter_joueur(self, joueur: Joueur):
        """
        Ajoute un joueur à la partie
        :param joueur:
        :return: True si le joueur a été ajouté, False sinon
        """
        if not self.partie_pleine():
            self.joueurs.add(joueur)

            if self.equipe_1.count() < 2:
                self.equipe_1.add(joueur)

            else:
                self.equipe_2.add(joueur)

            # si la partie est pleine, on passe à l'état ENCHERES
            if self.nombre_joueurs() == 4:
                self.etat = "ENCHERES"

                self.index_donneur = random.randint(0, 3)
                self.index_joueur_actuel = (self.index_donneur + 1) % 4
                self.distribuer()
            self.save()
            return True

        elif joueur in self.joueurs.all():
            if self.partie_pleine():
                self.etat = "ENCHERES"
                self.index_donneur = random.randint(0, 3)
                self.index_joueur_actuel = (self.index_donneur + 1) % 4
                self.distribuer()

            self.save()
            return True

        self.save()
        return False

    def partie_pleine(self):
        return self.nombre_joueurs() == 4

    def a_son_tour(self, joueur: Joueur):
        print(self.ordre_joueurs())
        return self.ordre_joueurs()[self.index_joueur_actuel] == joueur.username

    @staticmethod
    def partie_existe(code):
        """
        Vérifie si une partie existe avec le code donné
        :param code:
        :return: True si la partie existe, False sinon
        """
        return Partie.objects.filter(code=code).exists()

    @staticmethod
    def creer_partie(type="PUBLIQUE"):
        """
        Crée une partie avec un joueur
        :param joueur:
        :param type:
        :return: la partie créée
        """
        partie = Partie.objects.create(code=generer_code_partie(), type=type)
        partie.deck = Deck().json()
        partie.save()
        return partie

    def ordre_joueurs(self):
        """
        Renvoie l'ordre des joueurs.
        On commence par le donneur, puis on continue dans le sens des aiguilles d'une montre en alternant les équipes
        """
        return list(
            self.joueurs.all().order_by("username").values_list("username", flat=True)
        )
        ordre = []
        equipe1 = list(
            self.equipe_1.all().order_by("username").values_list("username", flat=True)
        )
        equipe2 = list(
            self.equipe_2.all().order_by("username").values_list("username", flat=True)
        )

        if self.donneur is not None and self.etat != "ATTENTE":
            if self.donneur.username in equipe1:
                index_donneur = equipe1.index(self.donneur.username)
                ordre.append(equipe2[index_donneur])
                ordre.append(equipe1[index_donneur - 1])
                ordre.append(equipe2[index_donneur - 1])
                ordre.append(equipe1[index_donneur])

            else:
                index_donneur = equipe2.index(self.donneur.username)
                ordre.append(equipe1[index_donneur])
                ordre.append(equipe2[index_donneur - 1])
                ordre.append(equipe1[index_donneur - 1])
                ordre.append(equipe2[index_donneur])
        else:
            ordre = equipe1 + equipe2
        return ordre

    def __str__(self):
        return f"Partie {self.code}"

    # ------- LOGIQUE DE JEU -------
    def distribuer(self):
        deck = Deck(self.deck)
        deck.couper()  # coupe le deck avant chaque distribution
        # Distribuer 3 cartes à chaque joueur, puis 2, puis encore 3
        nouvelle_main = {joueur.username: [] for joueur in self.joueurs.all()}
        for _ in range(3):
            for joueur in self.joueurs.all():
                nouvelle_main[joueur.username].append(deck.distribuer().json())
        for _ in range(2):
            for joueur in self.joueurs.all():
                nouvelle_main[joueur.username].append(deck.distribuer().json())
        for _ in range(3):
            for joueur in self.joueurs.all():
                nouvelle_main[joueur.username].append(deck.distribuer().json())

        self.joueurs_cartes = nouvelle_main

    def jouer_carte(self, joueur: Joueur, famille, valeur):
        """
        Le joueur joue une carte
        :param joueur:
        :param carte:
        :return:
        """
        logger.info(f"Joueur {joueur.username} joue {famille} {valeur}")
        # si le joueur peut jouer cette carte
        if joueur.peut_jouer(
            self.joueurs_cartes[joueur.username],
            self.pli_actuel,
            self.atout_actuel,
            Carte(famille, valeur),
        ):
            # on lui enlève la carte
            self.joueurs_cartes[joueur.username].remove(Carte(famille, valeur).json())

            # on ajoute la carte au pli
            self.pli_actuel["joueurs"][joueur.username] = Carte(famille, valeur).json()

            # on ajoute la carte aux cartes du pli
            self.pli_actuel["cartes"].append(Carte(famille, valeur).json())

            # si premier coup on sauvegarde la couleur du pli
            if len(self.pli_actuel["cartes"]) == 1:
                self.pli_actuel["couleur"] = famille

            # si tout le monde a joué
            if len(self.pli_actuel["cartes"]) >= 4:
                # si ce n'est pas le dernier pli on termine le pli
                if len(self.joueurs_cartes[joueur.username]) > 0:
                    self.terminer_pli()
                # sinon si les points sont inférieur à 700 ont continue la partie en commancant un nouveau tour
                elif self.pts_equipe_1 < POINTS_MAX and self.pts_equipe_2 < POINTS_MAX:
                    self.terminer_pli()
                    self.nouveau_tour()
                # sinon on termine la partie
                else:
                    logger.info("Partie terminée")
                    self.terminer_pli()
                    self.terminer_partie()
            else:
                self.avancer_tour()
        else:
            raise Exception("Pas autorisé à jouer cette carte")

    def annoncer_enchere(self, joueur: Joueur, enchere, atout: str):
        if not self.index_joueur_actuel == self.ordre_joueurs().index(joueur.username):
            raise Exception("Ce n'est pas à votre tour de jouer")

        if atout == "passer":
            self.encheres_joueurs[joueur.username] = {
                "enchere": 0,
                "atout": "passer",
            }
            self.avancer_tour()
        elif self._verifier_enchere(enchere):
            self.enchere = enchere
            self.atout_actuel = atout
            self.encheres_joueurs[joueur.username] = {
                "enchere": enchere,
                "atout": atout,
            }
            self.avancer_tour()

        else:
            raise Exception("Enchere Pas Valide")

        count_joueurs_passe = 0
        count_joueurs_enchere = 0

        for joueur in self.encheres_joueurs.keys():
            if self.encheres_joueurs[joueur]["atout"] == "passer":
                count_joueurs_passe += 1
            if self.encheres_joueurs[joueur]["atout"] != "passer":
                count_joueurs_enchere += 1

        if count_joueurs_passe == 3 and count_joueurs_enchere == 1:
            self.etat = "JEU"
            self.encheres_joueurs = {}

        if count_joueurs_passe >= 4:
            self.distribuer()
            self.encheres_joueurs = {}
            self.etat = "ENCHERES"
            self.avancer_donneur()

    def _verifier_enchere(self, enchere):
        """
        Vérifie si l'enchère est valide
        :param joueur:
        :param enchere:
        :return: True or False
        """
        valid = False
        if enchere >= 80 and enchere <= 500:
            if enchere >= self.enchere + 10:
                if enchere % 10 == 0:
                    valid = True
        return valid

    def avancer_tour(self):
        self.index_joueur_actuel = (self.index_joueur_actuel + 1) % 4

    def nouveau_tour(self):
        """
        Démarre un nouveau tour, redonne les cartes aux joueurs et change le donneur
        :return:
        """
        self.distribuer()
        self.avancer_donneur()
        self.encheres_joueurs = {}
        self.enchere = 0
        self.atout_actuel = None
        self.dernier_pli = {}
        self.pli_actuel = pli_default()
        self.etat = "ENCHERES"

    def avancer_donneur(self):
        self.index_donneur = (self.index_donneur + 1) % 4
        self.index_joueur_actuel = (self.index_donneur + 1) % 4

    def joueur_possede_carte(self, joueur, carte: Carte):
        """
        Vérifie si le joueur possède la carte
        :param joueur:
        :param carte:
        :return:
        """
        print(self.joueurs_cartes[joueur.username])
        print(carte.json())
        print(carte.json() in self.joueurs_cartes[joueur.username])
        return carte.json() in self.joueurs_cartes[joueur.username]

    def terminer_pli(self):
        """
        Termine le pli en cours, compte les points, et change le donneur au joueur gagnant.
        :return:
        """

        carte_gagnante = self.meilleur_carte_pli()
        joueurs_pli = self.pli_actuel.get("joueurs")
        username_joueur_gagnant = None
        # on itère sur les joueurs du pli et on récupère le joueur qui a joué la carte gagnante
        for joueur in joueurs_pli.keys():
            if Carte.from_json(joueurs_pli[joueur]) == carte_gagnante:
                username_joueur_gagnant = joueur
        logger.info("Terminer pli : " + username_joueur_gagnant)
        logger.info("carte gagnante : " + str(carte_gagnante))
        points_pli = self.points_pli()
        # on regarde dans la liste des joueurs, si index 0 ou 2, alors equipe 1, sinon equipe 2
        if self.ordre_joueurs().index(username_joueur_gagnant) % 2 == 0:
            self.pts_equipe_1 += points_pli
        else:
            self.pts_equipe_2 += points_pli

        self.dernier_pli = self.pli_actuel
        self.pli_actuel = pli_default()
        self.index_joueur_actuel = self.ordre_joueurs().index(username_joueur_gagnant)

    def terminer_partie(self):
        self.terminee = True
        self.etat = "TERMINEE"

        if self.pts_equipe_1 >= POINTS_MAX:
            gagnant = 0
        else:
            gagnant = 1

        # si gagnant == 0 ordre joueur 0 et 2 sinon 1 et 3
        joueur_gagnant = [
            self.ordre_joueurs()[gagnant],
            self.ordre_joueurs()[gagnant + 2],
        ]
        perdant = [
            self.ordre_joueurs()[gagnant + 1],
            self.ordre_joueurs()[gagnant + 3 % 4],
        ]
        points_gagnants = self.pts_equipe_1 if gagnant == 0 else self.pts_equipe_2
        points_perdants = self.pts_equipe_2 if gagnant == 0 else self.pts_equipe_1
        histo = HistoriquePartie.objects.create(
            points_gagnants=points_gagnants,
            points_perdants=points_perdants,
            partie=self,
        )
        histo.gagnants.set(Joueur.objects.filter(username__in=joueur_gagnant))
        histo.perdants.set(Joueur.objects.filter(username__in=perdant))

        histo.save()

    def meilleur_carte_pli(self) -> Carte:
        """
        Renvoie la meilleur carte du pli actuel
        :return: la carte la plus forte du pli
        """
        copy_pli = self.pli_actuel.copy()

        meilleur_carte = Carte.from_json(self.pli_actuel.get("cartes")[0])
        for carte in copy_pli.get("cartes"):
            if not meilleur_carte.superieur(
                Carte.from_json(carte),
                self.atout_actuel,
                self.pli_actuel.get("couleur"),
            ):
                meilleur_carte = Carte.from_json(carte)

        return meilleur_carte

    def points_pli(self) -> int:
        """
        Renvoie le nombre de points du pli actuel
        :return:
        """
        points = 0
        for carte in self.pli_actuel.get("cartes"):
            points += Carte.from_json(carte).valeur(self.atout_actuel)
        return points
