from typing import List

from WS_JEU.partie.carte import Carte
from random import shuffle as shuffle_list, randint

FAMILLES = ["coeur", "carreau", "pique", "trefle"]
VALEURS = [1, 7, 8, 9, 10, 11, 12, 13]


class Deck:
    def __init__(self, json_data=None):
        self._cards = []
        if json_data:
            for carte in json_data:
                self._cards.append(Carte.from_json(carte))
            return

        else:
            for famille in FAMILLES:
                for valeur in VALEURS:
                    self._cards.append(Carte(famille, valeur))

            self.melange()

    def from_json(data):
        return Deck(data)

    def melange(self):
        shuffle_list(self._cards)

    def couper(self):
        """
        Coupe le deck en deux
        """
        cut = randint(0, len(self._cards) - 10)
        self._cards = self._cards[cut:] + self._cards[:cut]

    def distribuer(self):
        """
        Distribue une carte
        :return:
        """
        return self._cards.pop()

    def set_cartes(self, cartes: List[Carte]):
        if len(cartes) != 32:
            raise Exception("Le deck doit contenir 32 cartes")

        self._cards = cartes

    def json(self):
        return [carte.json() for carte in self._cards]

    def __len__(self):
        return len(self._cards)

    def __str__(self):
        return str(self._cards)
