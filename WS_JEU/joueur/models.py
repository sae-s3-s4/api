# on importe le default USER de django, on l'étend pour créer la classe Joueur
from typing import List

from django.contrib.auth.models import User

from WS_JEU.partie.carte import Carte
import logging

logger = logging.getLogger(__name__)


class Joueur(User):
    """
    Définition de la classe Joueur qui étend la classe User de Django
    """

    class Meta:
        proxy = True

    def __str__(self):
        return self.username

    def possede_carte(self, main: List[dict], carte: Carte):
        """
        Retourne True si le joueur possède la carte dans sa main
        :param main: liste de cartes
        :param carte: carte
        :return: bool
        """
        for c in main:
            if Carte.from_json(c) == carte:
                return True
        return False

    def possede_carte_famille(self, main: List[dict], famille: str):
        """
        Retourne True si le joueur possède la carte dans sa main
        :param main: liste de cartes
        :param carte: carte
        :return: bool
        """
        for c in main:
            if Carte.from_json(c).get_famille() == famille:
                return True
        return False

    def possede_carte_famille_superieur(
        self, main: List[dict], atout: str, famille: str, carte: Carte
    ):
        """
        Retourne True si le joueur possède une carte supérieure à la carte de la meme famille "carte" dans sa main
        :param atout: atout actuel
        :param famille: famille de la carte
        :param main: liste de cartes
        :param carte: carte de référence
        :return: bool
        """
        for c in main:
            if Carte.from_json(c).get_famille() == famille and Carte.from_json(
                c
            ).superieur(carte, atout):
                return True
        return False

    def peut_jouer(
        self, main: List[dict], pli, atout: str, carte, check_deja_joue=True
    ):
        """
        Retourne True si le joueur peut jouer une carte.
        Pour ne pas avoir à vérifier toutes les conditions, on vérifie les conditions d'invalidité.
        A part pour les cas triviaux, comme le premier coup.

        :param main: liste de cartes
        :param pli: liste de cartes
        :param atout: atout actuel
        :param carte: carte la carte jouée
        :return: bool
        """
        # TODO , il manque la règle de la pissette
        famille_pli = pli.get("couleur")
        joueurs = pli.get("joueurs")
        cartes = pli.get("cartes")

        # a deja joué
        if self.username in joueurs.keys() and check_deja_joue:
            logger.info("le joueur a déjà joué")
            return False

        # n'a pas la carte
        if not self.possede_carte(main, carte):
            logger.info("le joueur ne possède pas la carte")
            return False

        # si c'est le premier joueur ok
        if len(cartes) == 0:
            logger.info("c'est le premier coup")
            return True

        # on ne recherche pas la carte gagnante si c'est le premier coup
        carte_gagnante = Carte.from_json(cartes[0])
        for c in cartes:
            _carte = Carte.from_json(c)
            if _carte.superieur(carte_gagnante, atout, famille_pli):
                carte_gagnante = _carte

        print("carte gagnante : " + str(carte_gagnante))

        # si meme couleur que le pli ok et qu'on joue pas atout
        # if carte.get_famille() == famille_pli and famille_pli != atout:
        #    print("condition 4")
        #    return True

        # si pas la meme couleur que le pli et qu'on possède la couleur du pli
        if carte.get_famille() != famille_pli and self.possede_carte_famille(
            main, famille_pli
        ):
            print("condition 5")
            return False

        # si le pli est atout et qu'on joue atout
        if famille_pli == atout and carte.get_famille() == atout:
            # si il joue une carte moins forte que la meilleure carte du pli mais qu'il possède une carte plus forte de la meme famille
            if carte_gagnante.superieur(
                carte, atout
            ) and self.possede_carte_famille_superieur(
                main, atout, famille_pli, carte_gagnante
            ):
                print("condition 6")
                return False

        print("on a tout testé, on retourne True")
        return True
