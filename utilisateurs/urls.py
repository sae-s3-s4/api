from django.urls import path, include

from WS_JEU.historique_partie.view import HistoriqueUtilisateurView, Classement
from WS_JEU.partie.views import CreationPartiePrivee, CreationPartiePublique, GetListeParties


urlpatterns = [
    path("auth/", include("utilisateurs.authentification.urls")),
    path("partie/", CreationPartiePrivee.as_view()),
    path("partie_publique/", CreationPartiePublique.as_view()),
    path("partie/historique/<str:username>/", HistoriqueUtilisateurView.as_view()),
    path("liste_parties/", GetListeParties.as_view()),
    path("classement/", Classement.as_view()),
]
