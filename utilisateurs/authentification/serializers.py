from django.contrib.auth.models import User
from rest_framework import serializers


class EnregistreUtilisateurSerializer(serializers.ModelSerializer):
    """Serializer pour créer un nouvel utilisateur"""

    class Meta:
        model = User
        fields = ("username", "password")
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        """
        Créer un nouvel utilisateur avec un mot de passe crypté et le retourner
        """
        utilistateur = User.objects.create_user(username=validated_data["username"])
        utilistateur.set_password(validated_data["password"])
        utilistateur.save()

        return utilistateur


class UtilisateurSerializer(serializers.ModelSerializer):
    """Serializer pour afficher les informations d'un utilisateur"""

    class Meta:
        model = User
        fields = ("username", "date_joined", "last_login")
