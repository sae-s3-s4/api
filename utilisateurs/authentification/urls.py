from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from utilisateurs.authentification.views import (
    EnregistreUtilisateurView,
    UtilisateurView,
)

urlpatterns = [
    path("token", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh", TokenRefreshView.as_view(), name="token_refresh"),
    path(
        "utilisateur",
        EnregistreUtilisateurView.as_view(),
        name="enregistre_utilisateur",
    ),
    path("utilisateur/infos/", UtilisateurView.as_view()),
]
