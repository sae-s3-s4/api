from rest_framework import generics
from utilisateurs.authentification.serializers import (
    EnregistreUtilisateurSerializer,
    UtilisateurSerializer,
)


class EnregistreUtilisateurView(generics.CreateAPIView):
    """Enregistre un nouvel utilisateur"""

    queryset = None
    serializer_class = EnregistreUtilisateurSerializer


class UtilisateurView(generics.RetrieveAPIView):
    """Affiche les informations d'un utilisateur connecté"""

    queryset = None
    serializer_class = UtilisateurSerializer

    def get_object(self):
        return self.request.user
