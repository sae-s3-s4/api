# API Coinchote
Ce repo contient tout le code nécessaire pour l'API Coinchote.

## Mise en place
### Prérequis
- Python 3.8 ou supérieur
- pip
- docker pour redis

### Installation
Cloner le repo
```bash
git clone https://forge.univ-lyon1.fr/sae-s3-s4/api.git
```

Installer les dépendances
```bash
pip install -r requirements.txt
```

Migration de la base de données
```bash
python manage.py migrate
```

Lancement REDIS
```bash
docker run --rm -p 6379:6379 redis:7
```

## Lancement
```bash
python manage.py runserver
```
## BUILD PUSH
```bash
docker buildx build --platform=linux/amd64 --push -t aimerisson/coinchotte-api:latest .
```

## Swagger
Après lancement de l'api, il est possible de consulter la documentation pour l'api REST via swagger !
http://localhost:8000/api/schema/swagger-ui